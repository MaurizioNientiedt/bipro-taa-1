# Will be set by Terraform Cloud
variable "name" {
  type = string
}

# Will be set by Terraform Cloud
variable "workspace_name" {
  type = string
}

# Will be set by Terraform Cloud
variable "aws_account_number" {
  type = string
}

variable "tags" {
  type = map(string)
  default = {
    TF-Managed  = true
    TF-Worfklow = "Terraform Cloud"
    Maintainer  = "adesso as a service GmbH"
  }
}
